use std::{
    io::{self, Write},
    path::{Path, PathBuf},
    time::{SystemTime, Duration},
};

use tempfile::TempDir;

use gpgme::{
    Context,
    EncryptFlags,
    Protocol,
};

// Public re-export for the convenience of end users.
pub use sop::{self, SOP};

use sop::{
    *,
    errors::*,
    ops::{
        EncryptAs,
        SignAs,
        Verification,
    },
    plumbing::PasswordsAreHumanReadable,
};

/// For debugging.
const KEEP_HOMEDIRS: bool = false;

pub struct Certs<'s> {
    gpgme: &'s GPGME,
    data: Vec<u8>,
}

impl<'s> sop::plumbing::SopRef<'s, GPGME> for Certs<'s> {
    fn sop(&self) -> &'s GPGME {
        self.gpgme
    }
}

impl<'s> sop::Load<'s, GPGME> for Certs<'s> {
    fn from_reader(gpgme: &'s GPGME, source: &mut (dyn io::Read + Send + Sync))
                   -> Result<Self>
    where
        Self: Sized,
    {
        let mut data = vec![];
        source.read_to_end(&mut data)?;
        Ok(Certs { gpgme, data, })
    }
}

impl sop::Save for Certs<'_> {
    fn to_writer(&self, armored: bool, sink: &mut (dyn io::Write + Send + Sync))
                 -> Result<()> {
        // Happy path: Want armored and it already is.
        if (armored && self.data.starts_with(
            b"-----BEGIN PGP PUBLIC KEY BLOCK-----"))
            || (! armored
                && self.data.get(0).map(|b| b & 0x80 == 0x80).unwrap_or(false))
        {
            sink.write_all(&self.data)?;
            return Ok(());
        }

        let (_gpg, mut ctx) = self.gpgme.context()?;
        ctx.import(&self.data).map_err(sop_error)?;

        let keys = ctx.keys().map_err(sop_error)?
            .map(|rk| rk.map_err(sop_error))
            .collect::<Result<Vec<_>>>()?;
        ctx.set_armor(armored);
        let mut sink =
            gpgme::Data::from_writer(sink).map_err(|_| Error::BadData)?;
        ctx.export_keys(&keys, gpgme::ExportMode::empty(), &mut sink)
            .map_err(sop_error)?;
        Ok(())
    }
}

pub struct Keys<'s> {
    gpgme: &'s GPGME,
    data: Vec<u8>,
}

impl<'s> sop::plumbing::SopRef<'s, GPGME> for Keys<'s> {
    fn sop(&self) -> &'s GPGME {
        self.gpgme
    }
}

impl<'s> sop::Load<'s, GPGME> for Keys<'s> {
    fn from_reader(gpgme: &'s GPGME, source: &mut (dyn io::Read + Send + Sync))
                   -> Result<Self>
    where
        Self: Sized,
    {
        let mut data = vec![];
        source.read_to_end(&mut data)?;
        Ok(Keys { gpgme, data, })
    }
}

impl sop::Save for Keys<'_> {
    fn to_writer(&self, armored: bool, sink: &mut (dyn io::Write + Send + Sync))
                 -> Result<()> {
        // Happy path: Want armored and it already is.
        if (armored && self.data.starts_with(
            b"-----BEGIN PGP SECRET KEY BLOCK-----"))
            || (! armored
                && self.data.get(0).map(|b| b & 0x80 == 0x80).unwrap_or(false))
        {
            sink.write_all(&self.data)?;
            return Ok(());
        }

        let (_gpg, mut ctx) = self.gpgme.context()?;
        ctx.import(&self.data).map_err(sop_error)?;

        let keys = ctx.keys().map_err(sop_error)?
            .map(|rk| rk.map_err(sop_error))
            .collect::<Result<Vec<_>>>()?;
        ctx.set_armor(armored);
        let mut sink =
            gpgme::Data::from_writer(sink).map_err(|_| Error::BadData)?;
        ctx.export_keys(&keys, gpgme::ExportMode::SECRET, &mut sink)
            .map_err(sop_error)?;
        Ok(())
    }
}

pub struct Sigs<'s> {
    gpgme: &'s GPGME,
    data: Vec<u8>,
}

impl<'s> sop::plumbing::SopRef<'s, GPGME> for Sigs<'s> {
    fn sop(&self) -> &'s GPGME {
        self.gpgme
    }
}

impl<'s> sop::Load<'s, GPGME> for Sigs<'s> {
    fn from_reader(gpgme: &'s GPGME, source: &mut (dyn io::Read + Send + Sync))
                   -> Result<Self>
    where
        Self: Sized,
    {
        let mut data = vec![];
        source.read_to_end(&mut data)?;
        Ok(Sigs { gpgme, data, })
    }
}

impl sop::Save for Sigs<'_> {
    fn to_writer(&self, armored: bool, sink: &mut (dyn io::Write + Send + Sync))
                 -> Result<()> {
        // Happy path: Want armored and it already is (because we
        // always armor the signatures).
        if armored {
            assert!(self.data.starts_with(b"-----BEGIN PGP SIGNATURE-----"));
            sink.write_all(&self.data)?;
        } else {
            self.gpgme.dearmor()?
                .data(&mut io::Cursor::new(&self.data))?
                .to_writer(sink)?;
        }
        Ok(())
    }
}

pub struct GPGME {
    gpg_path: PathBuf,
}

impl Default for GPGME {
    fn default() -> Self {
        Self::with_gpg_path(
            std::env::var("GNUPG_BIN").unwrap_or("/usr/bin/gpg".into()))
    }
}

impl GPGME {
    /// Creates a [`sop::SOP`] implementation with an explicit
    /// path to the GnuPG executable.
    ///
    /// To use either the engine specified by the environment variable
    /// `GNUPG_BIN` or `/usr/bin/gpg`, use `GPGME::default()`.
    pub fn with_gpg_path<P: AsRef<Path>>(path: P) -> Self {
        GPGME {
            gpg_path: path.as_ref().into(),
        }
    }

    fn context(&self) -> Result<(GnuPG, gpgme::Context)> {
        let gpg = GnuPG::new(&self.gpg_path)?;
        let gpgme = gpg.ctx()?;
        Ok((gpg, gpgme))
    }
}

impl<'s> sop::SOP<'s> for GPGME {
    type Keys = Keys<'s>;
    type Certs = Certs<'s>;
    type Sigs = Sigs<'s>;

    fn version(&'s self) -> Result<Box<dyn sop::ops::Version + 's>> {
        Version::new(self)
    }
    fn generate_key(&'s self)
        -> Result<Box<dyn sop::ops::GenerateKey<GPGME, Keys<'s>> + 's>>
    {
        GenerateKey::new(self)
    }
    fn change_key_password(&'s self)
        -> Result<Box<dyn sop::ops::ChangeKeyPassword<GPGME, Keys<'s>> + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn revoke_key(&'s self)
        -> Result<Box<dyn sop::ops::RevokeKey<GPGME, Certs, Keys<'s>> + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn extract_cert(&'s self)
        -> Result<Box<dyn sop::ops::ExtractCert<GPGME, Certs, Keys<'s>> + 's>>
    {
        ExtractCert::new(self)
    }
    fn sign(&'s self)
        -> Result<Box<dyn sop::ops::Sign<GPGME, Keys, Sigs> + 's>>
    {
        Sign::new(self)
    }
    fn verify(&'s self)
        -> Result<Box<dyn sop::ops::Verify<GPGME, Certs, Sigs> + 's>>
    {
        Verify::new(self)
    }
    fn encrypt(&'s self)
        -> Result<Box<dyn sop::ops::Encrypt<GPGME, Certs, Keys<'s>> + 's>> {
        Encrypt::new(self)
    }
    fn decrypt(&'s self)
        -> Result<Box<dyn sop::ops::Decrypt<GPGME, Certs, Keys<'s>> + 's>> {
        Decrypt::new(self)
    }
    fn armor(&'s self) -> Result<Box<dyn sop::ops::Armor + 's>> {
        Err(Error::NotImplemented)
    }
    fn dearmor(&'s self) -> Result<Box<dyn sop::ops::Dearmor + 's>> {
        Dearmor::new(self)
    }
    fn inline_detach(&'s self)
        -> Result<Box<dyn sop::ops::InlineDetach<Sigs> + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn inline_verify(&'s self)
        -> Result<Box<dyn sop::ops::InlineVerify<GPGME, Certs> + 's>>
    {
        Err(Error::NotImplemented)
    }
    fn inline_sign(&'s self)
        -> Result<Box<dyn sop::ops::InlineSign<GPGME, Keys<'s>> + 's>>
    {
        Err(Error::NotImplemented)
    }
}

struct Version {
    gpg: GnuPG,
}

impl<'s> Version {
    fn new(gpgme: &'s GPGME) -> Result<Box<dyn sop::ops::Version<'s> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        Ok(Box::new(Self {
            gpg,
        }))
    }
}

impl<'s> sop::ops::Version<'s> for Version {
    fn frontend(&self) -> Result<sop::ops::VersionInfo> {
        Ok(sop::ops::VersionInfo {
            name: "gpgme-sop".into(),
            version: env!("CARGO_PKG_VERSION").into(),
        })
    }

    fn backend(&self) -> Result<sop::ops::VersionInfo> {
        Ok(sop::ops::VersionInfo {
            name: "GnuPG".into(),
            version: self.gpg.ctx()?.engine_info().version().unwrap().into(),
        })
    }

    fn extended(&self) -> Result<String> {
        Ok(self.backend()?.to_string())
    }
}

struct GenerateKey<'s> {
    gpgme: &'s GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    signing_only: bool,
    keypassword: Option<Password>,
    userids: Vec<String>,
}

impl<'s> GenerateKey<'s> {
    fn new(gpgme: &'s GPGME) -> Result<Box<dyn sop::ops::GenerateKey<'s, GPGME, Keys<'s>> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            signing_only: false,
            keypassword: None,
            userids: Default::default(),
        }))
    }
}

impl<'s> sop::ops::GenerateKey<'s, GPGME, Keys<'s>> for GenerateKey<'s> {
    fn signing_only(mut self: Box<Self>)
                    -> Box<dyn sop::ops::GenerateKey<'s, GPGME, Keys<'s>> + 's> {
        self.signing_only = true;
        self
    }

    fn userid(mut self: Box<Self>, userid: &str)
              -> Box<dyn sop::ops::GenerateKey<'s, GPGME, Keys<'s>> + 's> {
        self.userids.push(userid.into());
        self
    }

    fn with_key_password(mut self: Box<Self>, keypassword: Password)
                         -> Result<Box<dyn sop::ops::GenerateKey<'s, GPGME, Keys<'s>> + 's>> {
        self.keypassword = Some(keypassword);
        Ok(self)
    }

    fn generate(self: Box<Self>) -> Result<Keys<'s>> {
        if self.userids.len() == 0 {
            return Err(Error::MissingArg);
        }

        let flags;
        let mut ctx;
        match self.keypassword {
            Some(p) => {
                flags = gpgme::CreateKeyFlags::empty();
                ctx = self.ctx.set_passphrase_provider(move |_: gpgme::PassphraseRequest,
                                                       w: &mut dyn Write| {
                                                           Ok(w.write_all(p.normalized()).unwrap())
                                                       });
            },
            None => {
                flags = gpgme::CreateKeyFlags::NOPASSWD;
                ctx = self.ctx.into();
            },
        }

        ctx.set_armor(true);
        let r = ctx.create_key_with_flags(
            &self.userids[0],
            "default",
            Duration::default(),
            flags).map_err(sop_error)?;
        let fp = r.fingerprint().unwrap();
        let key = ctx.get_key(fp).map_err(sop_error)?;
        for u in &self.userids[1..] {
            ctx.add_uid(&key, u).map_err(sop_error)?;
        }

        let mut data = Vec::new();
        ctx.export_keys(&[key], gpgme::ExportMode::SECRET, &mut data)
            .map_err(sop_error)?;

        Ok(Keys { gpgme: self.gpgme, data, })
    }
}

struct ExtractCert<'s> {
    gpgme: &'s GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
}

impl<'s> ExtractCert<'s> {
    fn new(gpgme: &'s GPGME) -> Result<Box<dyn sop::ops::ExtractCert<'s, GPGME, Certs, Keys<'s>> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let mut ctx = gpg.ctx()?;
        ctx.set_armor(true);
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
        }))
    }
}

impl<'s> sop::ops::ExtractCert<'s, GPGME, Certs<'s>, Keys<'s>> for ExtractCert<'s> {
    fn keys(mut self: Box<Self>, keys: &Keys) -> Result<Certs<'s>> {
        self.ctx.import(&keys.data).map_err(sop_error)?;
        let keys = self.ctx.keys().map_err(sop_error)?
            .map(|rk| rk.map_err(sop_error))
            .collect::<Result<Vec<_>>>()?;

        let mut data = Vec::new();
        self.ctx.export_keys(&keys, gpgme::ExportMode::empty(), &mut data)
            .map_err(sop_error)?;

        Ok(Certs { gpgme: self.gpgme, data, })
    }
}

struct Sign<'s> {
    #[allow(dead_code)]
    gpgme: &'s GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    keypasswords: Vec<Password>,
}

impl<'s> Sign<'s> {
    fn new(gpgme: &'s GPGME)
           -> Result<Box<dyn sop::ops::Sign<'s, GPGME, Keys<'s>, Sigs<'s>> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let mut ctx = gpg.ctx()?;
        ctx.set_armor(true);
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            keypasswords: Vec::new(),
        }))
    }
}

impl<'s> sop::ops::Sign<'s, GPGME, Keys<'s>, Sigs<'s>> for Sign<'s> {
    fn mode(mut self: Box<Self>, mode: SignAs)
            -> Box<dyn sop::ops::Sign<'s, GPGME, Keys<'s>, Sigs<'s>> + 's> {
        if let SignAs::Text = mode {
            self.ctx.set_text_mode(true);
        }
        self
    }

    fn with_key_password(mut self: Box<Self>, keypassword: Password)
              -> Result<Box<dyn sop::ops::Sign<'s, GPGME, Keys<'s>, Sigs<'s>> + 's>> {
        if ! self.keypasswords.is_empty() {
            // Signing with more than one key that needs a password is not
            // supported (We do not know how to make that happen)
            return Err(Error::NotImplemented);
        }
        self.keypasswords.push(keypassword);
        Ok(self)
    }

    fn keys(mut self: Box<Self>, keys: &Keys)
            -> Result<Box<dyn sop::ops::Sign<'s, GPGME, Keys<'s>, Sigs<'s>> + 's>> {
        for import in self.ctx.import(&keys.data).map_err(sop_error)?.imports()
        {
            // Handle import errors.
            import.result().map_err(sop_error)?;

            let key = self.ctx.get_key(import.fingerprint().unwrap())
                .expect("just imported");
            self.ctx.add_signer(&key).map_err(sop_error)?;
        }

        Ok(self)
    }

    fn data(self: Box<Self>, data: &mut (dyn io::Read + Send + Sync))
            -> Result<(sop::ops::Micalg, Sigs<'s>)>
    {
        let mut xxx = Vec::new();
        data.read_to_end(&mut xxx)?;
        let mut ctx = match self.keypasswords.len() {
            0 => self.ctx.into(),
            1 => {
                let mypass = &self.keypasswords[0];
                self.ctx.set_passphrase_provider(
                    move |_: gpgme::PassphraseRequest,
                    w: &mut dyn Write| {
                        Ok(w.write_all(mypass.normalized()).unwrap())
                    })
            },
            // FIXME: handle more than one password somehow
            _ => return Err(Error::UnsupportedOption),
        };

        let mut sig = Vec::new();
        let result =
            ctx.sign(gpgme::SignMode::Detached, &xxx, &mut sig)
            .map_err(sop_error)?;
        Ok((result.new_signatures().next()
            .map(|sig| (sig.hash_algorithm().raw() as u8).into())
            .unwrap_or(sop::ops::Micalg::Unknown("unknown".into())),
            Sigs {
                gpgme: self.gpgme,
                data: sig,
            }))
    }
}

struct Verify<'s> {
    #[allow(dead_code)]
    gpgme: &'s GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    not_before: Option<SystemTime>,
    not_after: Option<SystemTime>,
}

impl<'s> Verify<'s> {
    fn new(gpgme: &'s GPGME)
           -> Result<Box<dyn sop::ops::Verify<'s, GPGME, Certs<'s>, Sigs<'s>> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            not_before: None,
            not_after: None,
        }))
    }
}

impl<'s> sop::ops::Verify<'s, GPGME, Certs<'s>, Sigs<'s>> for Verify<'s> {
    fn not_before(mut self: Box<Self>, t: SystemTime)
                  -> Box<dyn sop::ops::Verify<'s, GPGME, Certs<'s>, Sigs<'s>> + 's> {
        self.not_before = Some(t);
        self
    }

    fn not_after(mut self: Box<Self>, t: SystemTime)
                 -> Box<dyn sop::ops::Verify<'s, GPGME, Certs<'s>, Sigs<'s>> + 's> {
        self.not_after = Some(t);
        self
    }

    fn certs(mut self: Box<Self>, certs: &Certs)
            -> Result<Box<dyn sop::ops::Verify<'s, GPGME, Certs<'s>, Sigs<'s>> + 's>> {
        for import in self.ctx.import(&certs.data).map_err(sop_error)?.imports()
        {
            // Handle import errors.
            import.result().map_err(sop_error)?;
        }
        Ok(self)
    }

    fn signatures<'sigs>(self: Box<Self>, signatures: &'sigs Sigs)
                         -> Result<Box<dyn sop::ops::VerifySignatures<'sigs> + 'sigs>>
    where
        's: 'sigs,
    {
        Ok(Box::new(VerifySignatures {
            verify: *self,
            signatures: signatures,
        }))
    }
}

struct VerifySignatures<'s, 'sigs> {
    verify: Verify<'s>,
    signatures: &'sigs Sigs<'s>,
}

impl sop::ops::VerifySignatures<'_> for VerifySignatures<'_, '_> {
    fn data(mut self: Box<Self>, data: &mut (dyn io::Read + Send + Sync))
            -> Result<Vec<sop::ops::Verification>> {
        let mut xxx = Vec::new();
        data.read_to_end(&mut xxx)?;
        let sigs = self.verify.ctx.verify_detached(&self.signatures.data, xxx)
            .map_err(sop_error)?;
        let verifications =
            sigs2verifications(sigs.signatures(),
                               self.verify.not_before, self.verify.not_after)?;

        if verifications.is_empty() {
            Err(Error::NoSignature)
        } else {
            Ok(verifications)
        }
    }
}

/// Converts `gpgme::Signature`s to `sop::ops::Verification`s.
fn sigs2verifications<'s>(sigs: impl IntoIterator<Item = gpgme::Signature<'s>>,
                          not_before: Option<SystemTime>,
                          not_after: Option<SystemTime>)
                          -> Result<Vec<sop::ops::Verification>> {
    let mut verifications = Vec::new();
    for sig in sigs {
        if sig.status().is_err()
        // XXX: Status is never GOOD or GREEN for me, despite
        // always-trust, so checking for RED instead.
            || sig.summary().contains(
                gpgme::SignatureSummary::RED)
        {
            continue;
        }

        let creation_time =
            if let Some(t) = sig.creation_time() {
                t
            } else {
                continue;
            };

        if let Some(not_before) = not_before {
            if creation_time < not_before {
                eprintln!(
                    "Signature by {} was created before \
                     the --not-before date.",
                    sig.fingerprint().unwrap());
                continue;
            }
        }

        if let Some(not_after) = not_after {
            if creation_time > not_after {
                eprintln!(
                    "Signature by {} was created after \
                     the --not-after date.",
                    sig.fingerprint().unwrap());
                continue;
            }
        }

        verifications.push(Verification::new(
            creation_time,
            sig.fingerprint().unwrap(),
            // XXX: Is this right?  Is sig.key() None if
            // the primary key is the signer?  Or is
            // something else wrong here?
            sig.key()
                .map(|key| key.fingerprint().unwrap().to_string())
                .unwrap_or(sig.fingerprint().unwrap().to_string()),
            sop::ops::SignatureMode::Binary, // XXX: GPGME doesn't provide this.
            None,
        )?);
    }
    Ok(verifications)
}

struct Encrypt<'s> {
    #[allow(dead_code)]
    gpgme: &'s GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    passwords: Vec<Password>,
    keypasswords: Vec<Password>,
    recipients: Vec<gpgme::keys::Key>,
    do_sign: bool,
}

impl<'s> Encrypt<'s> {
    fn new(gpgme: &'s GPGME) -> Result<Box<dyn sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            passwords: Vec::new(),
            keypasswords: Vec::new(),
            recipients: Vec::new(),
            do_sign: false,
        }))
    }
}

impl<'s> sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> for Encrypt<'s> {
    /// Disables armor encoding.
    fn no_armor(mut self: Box<Self>) -> Box<dyn sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's> {
        self.ctx.set_armor(false);
        self
    }

    /// Sets encryption mode.
    fn mode(mut self: Box<Self>, mode: EncryptAs) -> Box<dyn sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's> {
        if let EncryptAs::Text = mode {
            self.ctx.set_text_mode(true);
            // XXX: Can we control the literal packets data type
            // field?
        }
        self
    }

    fn sign_with_keys(mut self: Box<Self>,
                      keys: &Keys)
                      -> Result<Box<dyn sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        for import in self.ctx.import(&keys.data).map_err(sop_error)?.imports()
        {
            // Handle import errors.
            import.result().map_err(sop_error)?;

            let key = self.ctx.get_key(import.fingerprint().unwrap())
                .expect("just imported");
            self.ctx.add_signer(&key).map_err(sop_error)?;
            self.do_sign = true;
        }

        Ok(self)
    }

    /// Encrypts with the given password.
    fn with_password(mut self: Box<Self>, password: Password)
                     -> Result<Box<dyn sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        if ! self.passwords.is_empty() {
            // Encrypting with more than one password is not
            // supported.
            return Err(Error::NotImplemented);
        }
        if ! self.keypasswords.is_empty() {
            // Encrypting with a password while signing with a locked
            // key is not supported.
            return Err(Error::NotImplemented);
        }

        self.passwords.push(password);
        Ok(self)
    }

    fn with_certs(mut self: Box<Self>, certs: &Certs)
                  -> Result<Box<dyn sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        for import in self.ctx.import(&certs.data).map_err(sop_error)?.imports()
        {
            // Handle import errors.
            import.result().map_err(sop_error)?;

            let key = self.ctx.get_key(import.fingerprint().unwrap())
                .expect("just imported");
            self.recipients.push(key);
        }
        Ok(self)
    }

    fn with_key_password(mut self: Box<Self>, keypassword: Password)
                         -> Result<Box<dyn sop::ops::Encrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        if ! self.keypasswords.is_empty() {
            // Signing with more than one locked key that needs a
            // password is not supported.
            return Err(Error::NotImplemented);
        }
        if ! self.passwords.is_empty() {
            // Encrypting with a password while signing with a locked
            // key is not supported.
            return Err(Error::NotImplemented);
        }

        self.keypasswords.push(keypassword);
        Ok(self)
    }

    /// Encrypts the given data yielding the ciphertext.
    fn plaintext<'d>(self: Box<Self>,
                     plaintext: &'d mut (dyn io::Read + Send + Sync))
                     -> Result<Box<dyn sop::ops::Ready<Option<SessionKey>> + 'd>>
    where
        's: 'd,
    {
        Ok(Box::new(EncryptReady {
            encrypt: *self,
            plaintext,
        }))
    }
}

struct EncryptReady<'s> {
    encrypt: Encrypt<'s>,
    plaintext: &'s mut (dyn io::Read + Send + Sync),
}

impl<'s> sop::ops::Ready<Option<SessionKey>> for EncryptReady<'s> {
    fn to_writer(mut self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<Option<SessionKey>>
    {
        let mut plaintext = Vec::new();
        self.plaintext.read_to_end(&mut plaintext)?;

        let mut flags = EncryptFlags::ALWAYS_TRUST;
        if ! self.encrypt.passwords.is_empty() {
            flags |= EncryptFlags::SYMMETRIC;
        }

        let password = if self.encrypt.passwords.is_empty() {
            if self.encrypt.keypasswords.is_empty() {
                None
            } else {
                Some(self.encrypt.keypasswords.swap_remove(0))
            }
        } else {
            if self.encrypt.keypasswords.is_empty() {
                Some(self.encrypt.passwords.swap_remove(0))
            } else {
                return Err(Error::NotImplemented);
            }
        };
        let mut ctx =
            if let Some(p) = password.as_ref()
        {
            self.encrypt.ctx
                .set_passphrase_provider(move |_: gpgme::PassphraseRequest,
                                         w: &mut dyn Write| {
                                             Ok(w.write_all(p.normalized()).unwrap())
                                         })
        } else {
            self.encrypt.ctx.into()
        };

        let mut ciphertext = Vec::new();
        if self.encrypt.recipients.is_empty() && password.is_none() {
            return Err(Error::MissingArg);
        } else if self.encrypt.recipients.is_empty() {
            if self.encrypt.do_sign {
                return Err(Error::NotImplemented);
            }
            ctx.encrypt_symmetric(plaintext, &mut ciphertext)
                .map_err(sop_error)?;
        } else if ! self.encrypt.do_sign {
            ctx.encrypt_with_flags(
                self.encrypt.recipients.iter(),
                plaintext,
                &mut ciphertext,
                flags)
                .map_err(sop_error)?;
        } else {
            ctx.sign_and_encrypt_with_flags(
                self.encrypt.recipients.iter(),
                plaintext,
                &mut ciphertext,
                flags)
                .map_err(sop_error)?;
        }
        sink.write_all(&ciphertext)?;

        Ok(None)
    }
}

struct Decrypt<'s> {
    #[allow(dead_code)]
    gpgme: &'s GPGME,
    #[allow(dead_code)]
    gpg: GnuPG,
    ctx: Context,
    not_before: Option<SystemTime>,
    not_after: Option<SystemTime>,
    passwords: Vec<Password>,
    keypasswords: Vec<Password>,
}

impl<'s> Decrypt<'s> {
    fn new(gpgme: &'s GPGME) -> Result<Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        let ctx = gpg.ctx()?;
        Ok(Box::new(Self {
            gpgme,
            gpg,
            ctx,
            not_before: None,
            not_after: None,
            passwords: Vec::new(),
            keypasswords: Vec::new(),
        }))
    }
}

impl<'s> sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> for Decrypt<'s> {
    /// Makes SOP consider signatures before this date invalid.
    fn verify_not_before(mut self: Box<Self>, t: SystemTime)
                         -> Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's> {
        self.not_before = Some(t);
        self
    }

    /// Makes SOP consider signatures after this date invalid.
    fn verify_not_after(mut self: Box<Self>, t: SystemTime)
                        -> Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's> {
        self.not_after = Some(t);
        self
    }

    fn verify_with_certs(mut self: Box<Self>,
                         certs: &Certs)
                         -> Result<Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        for import in self.ctx.import(&certs.data).map_err(sop_error)?.imports()
        {
            // Handle import errors.
            import.result().map_err(sop_error)?;
        }

        Ok(self)
    }

    /// Tries to decrypt with the given session key.
    fn with_session_key(self: Box<Self>, _sk: SessionKey)
                        -> Result<Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        Err(Error::NotImplemented)
    }

    /// Tries to decrypt with the given password.
    fn with_password(mut self: Box<Self>, password: Password)
                     -> Result<Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        if ! self.passwords.is_empty() {
            // Decrypting with more than one password is not yet supported
            // (see XXX below)
            return Err(Error::NotImplemented);
        }
        if ! self.keypasswords.is_empty() {
            // Decrypting with a password while also decrypting with a
            // locked key is not supported (see XXX below).
            return Err(Error::NotImplemented);
        }
        self.passwords.push(password);
        Ok(self)
    }

    fn with_keys(mut self: Box<Self>, keys: &Keys)
                 -> Result<Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        for import in self.ctx.import(&keys.data).map_err(sop_error)?.imports()
        {
            // Handle import errors.
            import.result().map_err(sop_error)?;
        }
        Ok(self)
    }

    fn with_key_password(mut self: Box<Self>, keypassword: Password)
                         -> Result<Box<dyn sop::ops::Decrypt<'s, GPGME, Certs<'s>, Keys<'s>> + 's>> {
        if ! self.keypasswords.is_empty() {
            // Decrypting with more than one locked key that needs a
            // password is not supported.
            return Err(Error::NotImplemented);
        }
        if ! self.passwords.is_empty() {
            // Decrypting with a password while also decrypting with a
            // locked key is not supported.
            return Err(Error::NotImplemented);
        }

        self.keypasswords.push(keypassword);
        Ok(self)
    }

    /// Decrypts `ciphertext`, returning verification results and
    /// plaintext.
    fn ciphertext<'d>(self: Box<Self>,
                      ciphertext: &'d mut (dyn io::Read + Send + Sync))
                      -> Result<Box<dyn sop::ops::Ready<(Option<SessionKey>,
                                                              Vec<sop::ops::Verification>)> + 'd>>
    where
        's: 'd,
    {
        Ok(Box::new(DecryptReady {
            decrypt: *self,
            ciphertext,
        }))
    }
}

struct DecryptReady<'s> {
    decrypt: Decrypt<'s>,
    ciphertext: &'s mut (dyn io::Read + Send + Sync),
}

impl<'s> sop::ops::Ready<(Option<SessionKey>, Vec<sop::ops::Verification>)>
    for DecryptReady<'s>
{
    fn to_writer(self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                -> Result<(Option<SessionKey>, Vec<sop::ops::Verification>)> {
        let mut passwords = self.decrypt.passwords;
        let mut keypasswords = self.decrypt.keypasswords;
        let mut ctx =
            self.decrypt.ctx.set_passphrase_provider(
                move |_: gpgme::PassphraseRequest, w: &mut dyn Write| {
                    // XXX: GPGME only polls once for the
                    // password, not sure why or what to do about
                    // it.
                    if let Some(p) = passwords.pop() {
                        w.write_all(p.normalized()).unwrap()
                    } else if let Some(p) = keypasswords.pop() {
                        w.write_all(p.normalized()).unwrap()
                    }
                    Ok(())
                });

        let mut ciphertext = Vec::new();
        self.ciphertext.read_to_end(&mut ciphertext)?;

        let mut plaintext = Vec::new();
        let (_, sigs) = ctx.decrypt_and_verify(ciphertext,
                                               &mut plaintext)
            .map_err(sop_error)?;
        let verifications =
            sigs2verifications(sigs.signatures(),
                               self.decrypt.not_before,
                               self.decrypt.not_after)?;

        sink.write_all(&plaintext)?;
        Ok((None, verifications))
    }
}

struct Dearmor<'s> {
    #[allow(dead_code)]
    gpgme: &'s GPGME,
    gpg: GnuPG,
}

impl<'s> Dearmor<'s> {
    fn new(gpgme: &'s GPGME) -> Result<Box<dyn sop::ops::Dearmor<'s> + 's>> {
        let gpg = GnuPG::new(&gpgme.gpg_path)?;
        Ok(Box::new(Dearmor {
            gpgme,
            gpg,
        }))
    }
}

impl<'s> sop::ops::Dearmor<'s> for Dearmor<'s> {
    fn data<'d>(self: Box<Self>, data: &'d mut (dyn io::Read + Send + Sync))
                -> Result<Box<dyn sop::ops::Ready + 'd>>
    where
        's: 'd
    {
        Ok(Box::new(DearmorReady {
            dearmor: *self,
            data,
        }))
    }
}

struct DearmorReady<'s> {
    dearmor: Dearmor<'s>,
    data: &'s mut (dyn io::Read + Send + Sync),
}

impl sop::ops::Ready for DearmorReady<'_> {
    fn to_writer(mut self: Box<Self>, sink: &mut (dyn io::Write + Send + Sync))
                 -> Result<()> {
        // Peek at the data to see if it is really armored.  We make
        // no effort to verify the packet structure.
        let mut first_byte = [0u8; 1];
        self.data.read_exact(&mut first_byte)
            .map_err(|_| Error::BadData)?;
        let armored = first_byte[0] & 0x80 == 0;

        if armored {
            std::thread::scope(|scope| -> Result<()> {
                // As GPGME has no interface for that, we invoke GnuPG
                // directly.
                let mut c = self.dearmor.gpg.command();
                c.arg("--dearmor");
                use std::process::Stdio;
                c.stdin(Stdio::piped());
                c.stdout(Stdio::piped());
                let mut child = c.spawn()?;

                let mut stdin = child.stdin.take().unwrap();
                let stdout = child.stdout.take().unwrap();

                // Start a thread to shuffle the binary data out.
                let copy_result = scope.spawn(move || {
                    let mut stdout = stdout;
                    std::io::copy(&mut stdout, sink)
                });

                // Write armored data to GnuPG.
                stdin.write_all(&first_byte)?;
                std::io::copy(&mut self.data, &mut stdin)?;
                drop(stdin);

                // Synchronize and handle errors.
                let child_result = child.wait()?;
                if ! child_result.success() {
                    return Err(Error::BadData);
                }
                copy_result.join().unwrap()?;
                Ok(())
            })?;
        } else {
            sink.write_all(&first_byte)?;
            std::io::copy(&mut self.data, sink)?;
        }
        Ok(())
    }
}

/// Maps Sequoia errors to Errors.
///
/// XXX: This is a bit of a friction point.  We likely need to improve
/// the SOP spec a little.
fn sop_error(e: gpgme::Error) -> Error {
    // XXX

    eprintln!("Warning: Untranslated error: {}", e);
    Error::BadData
}

struct GnuPG {
    engine: PathBuf,
    homedir: TempDir,
}

impl GnuPG {
    fn new<P: AsRef<Path>>(engine: P) -> Result<GnuPG> {
        let homedir = TempDir::new()?;
        if cfg!(unix) {
            use std::os::unix::fs::PermissionsExt;
            let mut p = homedir.path().metadata()?.permissions();
            p.set_mode(p.mode() & 0o700);
            std::fs::set_permissions(homedir.path(), p)?;
        }

        std::fs::write(homedir.path().join("gpg.conf"),
                       "batch\nalways-trust\n")?;
        std::fs::write(homedir.path().join("gpg-agent.conf"),
                       "allow-loopback-pinentry\n")?;
        Ok(GnuPG {
            engine: engine.as_ref().into(),
            homedir,
        })
    }

    fn ctx(&self) -> Result<Context> {
        let mut ctx = Context::from_protocol(Protocol::OpenPgp)
            .map_err(sop_error)?;
        ctx.set_pinentry_mode(gpgme::PinentryMode::Loopback)
            .map_err(sop_error)?;
        ctx.set_armor(true);
        ctx.set_engine_path(
            String::from(self.engine.to_str().unwrap()))
            .map_err(sop_error)?;
        ctx.set_engine_home_dir(
            String::from(self.homedir.path().to_string_lossy()))
            .map_err(sop_error)?;
        Ok(ctx)
    }

    fn command(&self) -> std::process::Command {
        let mut c = std::process::Command::new(&self.engine);
        c.arg("--homedir").arg(self.homedir.path());
        c
    }
}

impl Drop for GnuPG {
    fn drop(&mut self) {
        if KEEP_HOMEDIRS {
            let homedir =
                std::mem::replace(&mut self.homedir, TempDir::new().unwrap());
            eprintln!("Leaving GnuPG homedir {:?} for inspection",
                      homedir.into_path());
        }
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;
    use super::*;

    /// This is the example from the SOP spec:
    ///
    ///     sop generate-key "Alice Lovelace <alice@openpgp.example>" > alice.sec
    ///     sop extract-cert < alice.sec > alice.pgp
    ///
    ///     sop sign --as=text alice.sec < statement.txt > statement.txt.asc
    ///     sop verify announcement.txt.asc alice.pgp < announcement.txt
    ///
    ///     sop encrypt --sign-with=alice.sec bob.pgp < msg.eml > encrypted.asc
    ///     sop decrypt alice.sec < ciphertext.asc > cleartext.out
    #[test]
    fn sop_examples() -> Result<()> {
        let sop = GPGME::default();

        let alice_sec = sop.generate_key()?
            .userid("Alice Lovelace <alice@openpgp.example>")
            .generate()?;
        let alice_pgp = sop.extract_cert()?
            .keys(&alice_sec)?;

        let bob_sec = sop.generate_key()?
            .userid("Bob Babbage <bob@openpgp.example>")
            .generate()?;
        let bob_pgp = sop.extract_cert()?
            .keys(&bob_sec)?;

        let statement = b"Hello World :)";
        let mut data = Cursor::new(&statement);
        let (_micalg, signature) = sop.sign()?
            .mode(SignAs::Text)
            .keys(&alice_sec)?
            .data(&mut data)?;

        let verifications = sop.verify()?
            .certs(&alice_pgp)?
            .signatures(&signature)?
            .data(&mut Cursor::new(&statement))?;
        assert_eq!(verifications.len(), 1);

        let mut statement_cur = Cursor::new(&statement);
        let (_session_key, ciphertext) = sop.encrypt()?
            .sign_with_keys(&alice_sec)?
            .with_certs(&bob_pgp)?
            .plaintext(&mut statement_cur)?
            .to_vec()?;

        let mut ciphertext_cur = Cursor::new(&ciphertext);
        let (_, plaintext) = sop.decrypt()?
            .with_keys(&bob_sec)?
            .ciphertext(&mut ciphertext_cur)?
            .to_vec()?;
        assert_eq!(&plaintext, statement);

        Ok(())
    }
}
