//! An implementation of the Stateless OpenPGP Command Line Interface
//! using GPGME.
//!
//! This implements a subset of the [Stateless OpenPGP Command Line
//! Interface] using the GPGME OpenPGP implementation.
//!
//!   [Stateless OpenPGP Command Line Interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/

use gpgme_sop::GPGME;

fn main() {
    sop::cli::main(&GPGME::default());
}
